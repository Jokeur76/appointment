from flask_restful import Resource


class FeatureController(Resource):

    @app.route("/test/<int:feat_id>/", methods=['GET', 'PUT', 'DELETE'])
    def get_by_id(self, feat_id):
        """
        Retrieve, update or delete note instances.
        """
        # if request.method == 'PUT':
        #     note = str(request.data.get('text', ''))
        #     notes[feat_id] = note
        #     return note_repr(feat_id)
        #
        # elif request.method == 'DELETE':
        #     notes.pop(feat_id, None)
        #     return '', status.HTTP_204_NO_CONTENT

        # request.method == 'GET'
        # if feat_id not in notes:
        #     raise exceptions.NotFound()
        return feat_id
